package com.ordinariabiv;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ProgOrdinariaBivFinalApplication {

    public static void main(String[] args) {
        SpringApplication.run(ProgOrdinariaBivFinalApplication.class, args);
    }

}
