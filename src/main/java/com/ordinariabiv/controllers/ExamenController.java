package com.ordinariabiv.controllers;

import com.ordinariabiv.utils.database.MySQLConnection;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.sql.Connection;
import java.sql.SQLException;

@Controller
public class ExamenController {

    @Autowired
    MySQLConnection mySQLConnection;

    @GetMapping("/")
    @ResponseBody
    public String mainAction() throws SQLException {
        Connection connection = mySQLConnection.getConnection();
        return "hola_mundo conexion establecida " + connection.isValid(2);
    }

}
