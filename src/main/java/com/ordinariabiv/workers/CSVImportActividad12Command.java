package com.ordinariabiv.workers;
import com.ordinariabiv.ProgOrdinariaBivFinalApplication;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.WebApplicationType;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.stereotype.Component;

@Component
@ConditionalOnProperty(name = "actividad1-2")
public class CSVImportActividad12Command implements CommandLineRunner {

    public static void main(String[] args) {
        new SpringApplicationBuilder(ProgOrdinariaBivFinalApplication.class).web(WebApplicationType.NONE).run("--actividad1-2");
    }

    public void run(String... args) {
        System.out.println("Implementa el codigo de la actividad 1 y 2");
    }

}
