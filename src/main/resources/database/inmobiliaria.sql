CREATE DATABASE inmobiliaria;
USE inmobiliaria;

DROP TABLE IF EXISTS `clientes`;
CREATE TABLE `clientes` (
  `dni` varchar(9) NOT NULL,
  `nombre` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`dni`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

INSERT INTO `clientes` VALUES ('1111111C','Pepe Pérez'),('22222222A','María Martínez');

DROP TABLE IF EXISTS `pisos`;
CREATE TABLE `pisos` (
  `codigo` int(11) NOT NULL AUTO_INCREMENT,
  `direccion` varchar(100) DEFAULT NULL,
  `descripcion` varchar(100) DEFAULT NULL,
  `metros2` decimal(8,2) DEFAULT NULL,
  `precio` decimal(8,2) DEFAULT NULL,
  `tipo` enum('VPO','LIBRE') NOT NULL,
  PRIMARY KEY (`codigo`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4;

INSERT INTO `pisos` VALUES (1,'Calle de la pena','Muy amplio',85.00,60000.00,'VPO'),(2,'Pasaje del hueco','Garaje incluído',100.00,80000.00,'LIBRE');

DROP TABLE IF EXISTS `visitas`;
CREATE TABLE `visitas` (
  `cliente` varchar(9) NOT NULL,
  `piso` int(11) NOT NULL,
  PRIMARY KEY (`cliente`,`piso`),
  KEY `visitas_FK_1` (`piso`),
  CONSTRAINT `visitas_FK` FOREIGN KEY (`cliente`) REFERENCES `clientes` (`dni`) ON UPDATE CASCADE,
  CONSTRAINT `visitas_FK_1` FOREIGN KEY (`piso`) REFERENCES `pisos` (`codigo`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;